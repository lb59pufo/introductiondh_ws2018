### Syllabus

#### Lecturer

* Name: Dr. Thomas Köntges 
* Phone: 0341 - 97 32348
* Email: thomas.koentges@uni-leipzig.de
* Twitter: @ThomasKoentges

##### Office Hours

Tuesdays 11:00 - 13:00, Paulinum Raum P-618, on request

Online: GoogleHangout, thomas.koentges@gmail.com, on request

#### Class Resources

* GitLab: https://git.informatik.uni-leipzig.de/koentges/introductiondh_ws2018
* OLAT: https://olat.informatik.uni-leipzig.de/

#### Expectations for Class Participation

* Physical presence in the lectures
* Preparation for and active participation in the seminars
* The assignments given in the seminars are due 48h before the next seminar
* Handing-in of the project report by March 01, 2017 (Prüfungsleistung)

#### Venues

* V: Lecture, Felix-Klein-Hörsaal P501 P5.014, Mondays 1.15-2.45pm (currently 73 students)
* S: Seminar,
	* Group A, Rechnerlabor P801 P8.011, Tuesdays, 3.15-4.45pm (currently 39 students)
	* Group B, Seminarraum S214 S2.206, Tuesdays, 7.15-8.45pm (currently 19 students)
	* Group C, Seminarraum S214 S2.206, Thursdays, 7.15-8.45pm (currently 14 students)
* P: Praktikum / Project (Vorlesungsfreie Zeit: Group Work & Project Report)
* B: Break (Season's Greetings!)

#### Schedule

| Number | Form | Topic | When | Comments|
| ------ | ---- | ----- | ---- | ---- |
| 1 | V | [Zero-Class/ House-keeping / Greg's Welcome / Manuel's Welcome](https://docs.google.com/presentation/d/1dqF9OzMRJPgXJVdf9WWKKMe5wC0ATekJOdoj3Ihjtp0/edit?usp=sharing) | 15.10. ||
| 1 | S | How to Git, Github & Gitlab / Stackoverflow | 16./16./18.10 ||
| 2 | V | [Everything you wanted to know about DH](https://docs.google.com/presentation/d/1X9vHvzuOBRB2uz2XmbbQ-rBR7o-CNx1TsAesslpKDpM/edit?usp=sharing) | 22.10. ||
| 2 | S | [Introduction to APIs and Data formats](https://docs.google.com/presentation/d/1flspA_smXqIUC7oLeFhkyjvhjJORJKJu1gyvOoWqf9U/edit?usp=sharing) | 23./23./25.10 |Install [Open Refine](http://openrefine.org/download.html)|
| 3 | V | [Combining Qualitative and Quantitative Research Methods](https://docs.google.com/presentation/d/18rkMv4WPXJUMW4DO4JS2fGoBh62uP_ZhosG0w7v6bNg/edit?usp=sharing) | 29.10. ||
| 3 | S | Data Cleaning & Visualizations without programming languages | 30.10/30.10./1.11. |Install [Open Refine](http://openrefine.org/download.html), if you have ArchLinux [here](https://git.informatik.uni-leipzig.de/koentges/introductiondh_ws2018/blob/master/archLinuxOpenRefine.md); also install [Tableau Public](https://public.tableau.com/en-us/s/) and inform yourself about Google Fusion|
| 4 | V | [Which language do you speak? Overview of programming languages used in Digital Humanities](https://docs.google.com/presentation/d/1DoEALQ2HaKZY2pfWJdaPjJdw6KbI0mMl3OEJgp6WtLA/edit?usp=sharing) | 5.11. | **Location change: HS 8**|
| 4 | S | Data Structures & Programming languages | 6./6./8.11. ||
| 5 | V | Linked Data & Microservices in the Humanities | 12.11. | **Location change: HS 8** |
| 5 | S | Building a Microservice | 13./13./15.11 | please install [R](https://www.r-project.org), [RStudio](https://www.rstudio.com), [Go](https://golang.org)|
| 6 | V | Natural Language Processing for Historical Languages (and other languages too) | 19.11. ||
| 6 | S | Working with High-Dimensional Data | 20./20./22.11 ||
| 7 | V | Topic Modelling & Word Vector Embedding | 26.11. ||
| 7 | S | Topic Modelling | 27./27./29.11 ||
| 8* | P | Homework: Which DH Project is interesting to you? And why?| 4.12. | Dies Academicus |
| 8 | S | Student Presentations | 5./5./7.12 ||
| 9 | V | Challenges of Data Curation & Corpus Building | 10.12. | Thomas arrives from KCL (shouldn't, but could be a bit late) |
| 9 | S | Building a Data Reading Environment | 11./11./13.12 | **13.12. cancelled students should visit one of teh seminars on the 11th** |
| 10 | V | Managing Big DH Projects (Gregory Crane) | 17.12. ||
| 10 | S/P | Developing Project Ideas | 18./18./20.12 ||
| - | B | Break | 24.12.18--6.1.19 ||
| 11 | V | Working with Geospatial Data | 7.1. ||
| 11 | S | Working with Geospatial Data | 8./8./10.1 ||
| 12 | V | Social Network Analysis | 14.1. ||
| 12 | S | Social Network Analysis | 15./15./17.1 ||
| 13 | V | Citizen Science in DH (Maryam Foradi) | 21.1. | Thomas at NTU |
| 13 | S | Citizen Science | 22./22./24.1 | Thomas at NTU |
| 14 | V | Stats and Numbers in DH (Jochen Tiepmar) | 28.1. | Thomas at VUW |
| 14 | S | Stats and Numbers (Jochen Tiepmar)| 29./29./31.1. | Thomas at VUW |
| 15 | V | Online Class: Repetition | 4.2. | Video Class |
| 15 | S | Online Class: Repetition / Feedback | 5./5./7.2 | Video Conference |